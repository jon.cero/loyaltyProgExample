//
//  yourCardsVC.m
//  loyaltyProgExample
//
//  Created by PixelByte on 16/08/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

#import "yourCardsVC.h"

//cells and headers
#import "sectionHeaderView.h"
#import "showCardsTVC.h"

@interface yourCardsVC ()

@property (strong, nonatomic) NSMutableArray *cardsList;

@end

@implementation yourCardsVC

#pragma mark - VC control
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    [self contentTableViewInit];
    
    [self setContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - utils
- (void)setContent {
    self.cardsList = [[NSMutableArray alloc] initWithArray:@[@"one", @"two"]];
}

#pragma mark - content tableview
- (void)contentTableViewInit {
    self.yourCardsListTV.delegate = self;
    self.yourCardsListTV.dataSource = self;
    
    self.yourCardsListTV.allowMultipleSectionsOpen = YES;
    
    //self.yourCardsListTV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //[self.yourCardsListTV registerClass:[showCardsTVC class] forCellReuseIdentifier:@"cardCells"];
    [self.yourCardsListTV registerNib:[UINib nibWithNibName:@"sectionHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:@"sectionHeader"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.cardsList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"sectionHeader"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    showCardsTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"cardCells"];
    if (cell == nil) {
        cell = [[showCardsTVC alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:@"cardCells"];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
    return [self tableView:tableView heightForHeaderInSection:section];
}

#pragma mark - FZAccordionTableViewDelegate
- (void)tableView:(FZAccordionTableView *)tableView willOpenSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
    
}

- (void)tableView:(FZAccordionTableView *)tableView didOpenSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
    
}

- (void)tableView:(FZAccordionTableView *)tableView willCloseSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
    
}

- (void)tableView:(FZAccordionTableView *)tableView didCloseSection:(NSInteger)section withHeader:(UITableViewHeaderFooterView *)header {
    
}

#pragma mark - btn control
- (IBAction)backBtnC:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
