//
//  frontPageVC.m
//  loyaltyProgExample
//
//  Created by PixelByte on 15/08/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

#import "frontPageVC.h"

#import "frontPageContentTVC.h"

@interface frontPageVC ()

@end

@implementation frontPageVC {
    NSMutableArray *contentList;
}

#pragma mark - VC control
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    [self contentTableViewInit];
    
    [self setContent];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - utils
- (void)setContent {
    contentList = [[NSMutableArray alloc] initWithArray:@[@"What's New", @"Your cards"]];
}

#pragma mark - content tableview
- (void)contentTableViewInit {
    self.frontPageContentTV.delegate = self;
    self.frontPageContentTV.dataSource = self;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return contentList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    frontPageContentTVC *cell =  [tableView dequeueReusableCellWithIdentifier:@"contentCell"];
    
    cell.contentTitleLbl.text = contentList[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"whatsNewSegue" sender:nil];
    } else {
        [self performSegueWithIdentifier:@"yourCardsSegue" sender:nil];
    }
    
}

@end
