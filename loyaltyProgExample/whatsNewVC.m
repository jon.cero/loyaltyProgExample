//
//  whatsNewVC.m
//  loyaltyProgExample
//
//  Created by PixelByte on 16/08/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

#import "whatsNewVC.h"

@interface whatsNewVC ()

@end

@implementation whatsNewVC

#pragma mark - VC control
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    [self headerViewInit];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - header view
- (void)headerViewInit {
    
    self.textView.delegate = self;
    //self.textView.scrollEnabled = NO;
    [self.textView scrollRangeToVisible:NSMakeRange(0, 1)];
    
    [self.headerView setDelegate:self];
    [self.headerView setCollapsingConstraint:self.headerHeight];
    
    NSArray *attrs;
    attrs = @[
              [MGTransform transformAttribute:MGAttributeY byValue:-38.0],
              [MGTransform transformAttribute:MGAttributeWidth byValue:-20.0],
              [MGTransform transformAttribute:MGAttributeHeight byValue:-9.0],
              [MGTransform transformAttribute:MGAttributeFontSize byValue:-12.0]
              ];
    [self.headerView addTransformingSubview:self.titleLbl attributes:attrs];
}

#pragma mark - text view
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.headerView collapseWithScroll:scrollView];
    
    //NSLog(@"V:|-(%.2f)-header(%.2f)-(%.2f)-|", self.headerTop.constant, self.headerHeight.constant, self.textViewTop.constant);
    
    //NSLog(@"?? %f", scrollView.contentOffset.x);
}

#pragma mark - collapse header view delegate method
- (void)headerDidCollapseToOffset:(double)offset {
    NSLog(@"collapse %.4f", offset);
}

- (void)headerDidFinishCollapsing {
    NSLog(@"collapsed");
    //self.textView.scrollEnabled = YES;
}

- (void)headerDidExpandToOffset:(double)offset {
    NSLog(@"expand %.4f", offset);
}

- (void)headerDidFinishExpanding {
    NSLog(@"expanded");
}

#pragma mark - btn control
- (IBAction)backBtnC:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
