//
//  whatsNewVC.h
//  loyaltyProgExample
//
//  Created by PixelByte on 16/08/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGCollapsingHeaderView.h"

@interface whatsNewVC : UIViewController <MGCollapsingHeaderDelegate, UITextViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet MGCollapsingHeaderView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerTop;

@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewTop;

@property (strong, nonatomic) IBOutlet UIButton *backBtn;
- (IBAction)backBtnC:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end
