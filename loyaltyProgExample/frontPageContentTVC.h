//
//  frontPageContentTVC.h
//  loyaltyProgExample
//
//  Created by PixelByte on 15/08/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface frontPageContentTVC : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *contentTitleLbl;

@end
