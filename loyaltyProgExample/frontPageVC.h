//
//  frontPageVC.h
//  loyaltyProgExample
//
//  Created by PixelByte on 15/08/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface frontPageVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *frontPageContentTV;

@end
