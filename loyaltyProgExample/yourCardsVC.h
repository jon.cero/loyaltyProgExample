//
//  yourCardsVC.h
//  loyaltyProgExample
//
//  Created by PixelByte on 16/08/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

#import <UIKit/UIKit.h>

//utils
#import "FZAccordionTableView.h"

@interface yourCardsVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

//nav buttons
@property (strong, nonatomic) IBOutlet UIButton *backBtn;
- (IBAction)backBtnC:(id)sender;

//tableview
@property (strong, nonatomic) IBOutlet FZAccordionTableView *yourCardsListTV;

@end
