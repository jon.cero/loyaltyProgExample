//
//  ViewController.m
//  loyaltyProgExample
//
//  Created by PixelByte on 15/08/2017.
//  Copyright © 2017 PixelByte. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self performSelector:@selector(goToFrontPage) withObject:nil afterDelay:2];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goToFrontPage {
    [self performSegueWithIdentifier:@"frontPageSegue" sender:nil];
}

@end
